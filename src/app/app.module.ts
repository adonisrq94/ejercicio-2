import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import {UserLogInPage} from '../pages/user-log-in/user-log-in';
import {UserDashboardPage} from '../pages/user-dashboard/user-dashboard';

//servicios
import {LoginService} from '../services/login.service';
import {PublicationService} from '../services/publication.service';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    UserLogInPage,
    UserDashboardPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    UserLogInPage,
    UserDashboardPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LoginService,
    PublicationService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
