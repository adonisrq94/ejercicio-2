import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
  selector: 'page-user-dashboard',
  templateUrl: 'user-dashboard.html',
})
export class UserDashboardPage {

  public data:any={}
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    //resivimos los datos de las vacantes
    this.data=this.navParams.get("data");
  }


}
