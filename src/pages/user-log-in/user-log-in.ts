import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController
,LoadingController} from 'ionic-angular';
import {LoginService} from '../../services/login.service';
import {PublicationService} from '../../services/publication.service';
import {UserDashboardPage} from '../user-dashboard/user-dashboard';

import {User} from './user';

@IonicPage()
@Component({
  selector: 'page-user-log-in',
  templateUrl: 'user-log-in.html',
})
export class UserLogInPage {
  public loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });
    public user:User;
  constructor(public navCtrl: NavController, public navParams: NavParams
             ,public _loginService:LoginService,
              public _publicationService:PublicationService,
              public toastCtrl: ToastController,
              public loadingCtrl: LoadingController) {
      this.user = new User();

  }

//esta funcion se encarga de traer el token del servidor
login(){
  //levantamos un loader
  this.loading.present();
  this._loginService.login(this.user).subscribe(data =>{
     if (data && data.token){
         this.publication();


     }

  },//quitamos el loader
   error=>{this.loading.dismiss();this.presentToast()})

}

 // esta funcion trae las vacantes disponibles
  publication():any{
    this._publicationService.publication().subscribe(data =>{
      //usamos setRoot en vez de push ya que no nos interesa sobreponer a la pantalla
      // anterior la cual seria el login, ya que al iniciar sesion no deberia volver al login
      this.navCtrl.setRoot(UserDashboardPage,{data});
      //quitamos el loader
      this.loading.dismiss();
    },error=>{this.loading.dismiss();this.presentToast()})
  }

//levanta un toast cuando no logra iniciar sesion
 presentToast() {
  let toast = this.toastCtrl.create({
    message: 'Usurio o clave no son correctos',
    duration: 3000,
    position: 'top'
  });
  toast.present();
}







}
