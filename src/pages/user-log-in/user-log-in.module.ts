import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserLogInPage } from './user-log-in';


@NgModule({
  declarations: [
    UserLogInPage,
  ],
  imports: [
    IonicPageModule.forChild(UserLogInPage),
  ],
})
export class UserLogInPageModule {





}
