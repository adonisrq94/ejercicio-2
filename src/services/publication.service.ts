import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PublicationService {

    postsUrl:string = "https://apidev.alanajobs.com";


  constructor(private http:Http) {

  }

  //endpoint que trae las vacantes disponibles
  publication(){
    let jwt2 = this.headers();
    return this.http.get(`${this.postsUrl}/secure-company/publication/index`, jwt2).map( (response: Response) => {
      let apiData = response.json();
      // si es 200 devolvemos solo los datos requeridos
      if (apiData['code']==200) {
        return apiData['response'];

     }else
     return apiData;
    }).catch((error: any) => Observable.throw(error.json()));
  }

//crea la cabecera de la peticion con su token requerido
headers():RequestOptions{

  let _currentUser = JSON.parse(localStorage.getItem('currentUser'));
  let _id: string = '1';
  if (_currentUser && _currentUser.token) {
  let _headers = new Headers({ 'Authorization':'Bearer ' + _currentUser.token,
  'company': _id });
  return new RequestOptions({ headers: _headers });}


}







}
