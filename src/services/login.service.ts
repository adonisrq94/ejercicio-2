import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, RequestMethod,Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {User} from '../pages/user-log-in/user';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class LoginService {

  postsUrl:string = "https://apidev.alanajobs.com";


  constructor( private http:Http){

   }

  // endpont que se encarga de traer el token 
  login(user:User)  {

    let _body: string = `email=` + user.getEmail() + `&password=` + user.getPassword();
    let options = this.createHeader();
    return this.http.post(`${this.postsUrl}/secure-company/login_check`, _body, options).map( (response: Response) => {
      let apiData = response.json();
      if (apiData && apiData.token) {
          user.setToken(apiData.token);
          localStorage.setItem('currentUser', JSON.stringify(user));
     }
     return apiData;
    }).catch((error: any) => Observable.throw(error.json()));


}

 // crea la cabecera de la peticion
  createHeader(){
    let _headers: Headers = new Headers();
    let options = new RequestOptions({ method: RequestMethod.Post, headers: _headers });
     _headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return options;
  }









}
